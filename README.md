# DriveToPlayfab 1.0.0 #

Download files from Google Drive and upload them to Playfab or store local backups.

### How do I get set up? ###

* Follow [UnityGoogleDrive's configuration guide](https://github.com/Elringus/UnityGoogleDrive) to set up Google Drive OAuth authentication. Configuration file is currently under Assets/DriveToPlayfab/Common/Resources/GoogleDriveSettings.

### Setting up environments and other configs ###

* Standalone Build: Just build it and go to the configuration tab
* Editor Tool: Configuration file is under Assets/DriveToPlayfab/Environment_Editor/Resources/DriveToPlayfab. Funcionality to download/upload files is within the same scriptable object.

### Dependencies ###

* [UnityGoogleDrive](https://github.com/Elringus/UnityGoogleDrive) (Comunication with GoogleDrive)
* Playfab SDK (Upload API)
* [StandaloneFileSystem](https://github.com/gkngkc/UnityStandaloneFileBrowser) (Minor standalone build customization)
* Log Viewer (Build console)
* More Efficient coroutines PRO (Editor coroutines)
* TextMeshPro

### Who do I talk to? ###

* Repo owner or admin