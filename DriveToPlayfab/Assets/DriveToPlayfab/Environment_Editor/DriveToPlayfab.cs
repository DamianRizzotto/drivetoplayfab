﻿using _Scripts.Model;
using _Scripts.Services;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "DriveToPlayfab", menuName = "DriveToPlayfab/Create configuration file", order = 1)]
public class DriveToPlayfab : ScriptableObject
{
    private static DriveToPlayfab _instanceBacking;
    public static DriveToPlayfab Instance
    {
        get
        {
            if (_instanceBacking == null) CreateSingletonInstance();
            return _instanceBacking;
        }
        private set => _instanceBacking = value;
    }

    private static void CreateSingletonInstance()
    {
        if (_instanceBacking != null) return;
        var prefab = Resources.Load<DriveToPlayfab>("DriveToPlayfab");
        Instance = Instantiate(prefab);
    }
    
    public Config Config;
    
    public static void UpdatePlayfabFiles(PlayfabEnvironmentConfig environmentConfig)
    {
        DriveService.DownloadDrive(() =>
        {
            DriveService.UpdateFileContents(() =>
            {
                GoogleDriveConfig driveConfig = ServiceHelper.GetConfig().GoogleDriveConfig;
                PlayfabService.UploadFilesUnderFolder(driveConfig.Folder, driveConfig.FileList, environmentConfig, () => { });
            });
        });
    }
    
    public static void DownloadDriveToLocal()
    {
        DriveService.DownloadDrive(() =>
        {
            DriveService.UpdateFileContents(() =>
            {
                LocalBackupConfig localBackupConfig = ServiceHelper.GetConfig().LocalBackupConfig;
                DriveService.GetFilesUnderFolderId(localBackupConfig.SubFolderNameOrId, (kvp) =>
                {
                    LocalBackupService.SaveLocalBackupFiles(kvp.Value, kvp.Key, AssetDatabase.Refresh);
                });
            });
        });
    }
}
