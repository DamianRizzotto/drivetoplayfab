﻿using _Scripts.Model;
using UnityEditor;
using UnityEngine;

namespace _Scripts.Editor {
    [CustomEditor(typeof(DriveToPlayfab))]
    public class DriveToPlayfabEditor : UnityEditor.Editor {
 
        private DriveToPlayfab _driveToPlayfab;
 
        public void OnEnable()
        {
            _driveToPlayfab = (DriveToPlayfab)target;
        }
 
        public override void OnInspectorGUI()
        {
            if(GUILayout.Button($"Update local files")) {
                DriveToPlayfab.DownloadDriveToLocal();
            }
    
            GUILayout.Space(10);
            foreach (PlayfabEnvironmentConfig config in _driveToPlayfab.Config.PlayfabConfig.EnvironmentConfigs)
            {
                if(GUILayout.Button($"Update Playfab [{config.Name}]")) {
                    DriveToPlayfab.UpdatePlayfabFiles(config);
                }
            }
        
            GUILayout.Space(30);
            DrawDefaultInspector();
        }
    }
}
