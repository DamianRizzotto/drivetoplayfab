﻿using UnityEngine;

public interface IPoolObject<T> {

    void Initialize(T initData);

}
