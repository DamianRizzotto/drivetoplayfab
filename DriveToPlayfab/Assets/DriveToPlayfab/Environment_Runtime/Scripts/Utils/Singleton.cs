﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Component {

    private static T _instance;
    public static T Instance => _instance ? _instance : (_instance = FindObjectOfType<T>());

    protected static bool HasInstance() {
        return _instance != null;
    }

    protected virtual void Awake() {
        if (_instance == null) {
            _instance = this as T;
        } else if (_instance != this) {
            Destroy(gameObject);
        }
    }

    protected virtual void OnDestroy() {
        if (_instance == this) {
            _instance = null;
        }
    }
}
