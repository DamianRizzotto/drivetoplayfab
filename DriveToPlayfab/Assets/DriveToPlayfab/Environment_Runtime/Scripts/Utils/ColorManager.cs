﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ColorManager
{
    public static Color Blue = new Color(0.22f, 0.68f, 0.85f);
    public static Color Purple = new Color(0.65f, 0.4f, 0.77f);
    public static Color Green = new Color(0.6f, 0.77f, 0.11f);
    public static Color Orange = new Color(0.95f, 0.73f, 0.24f);
    public static Color Red = new Color(0.95f, 0.34f, 0.25f);
}
