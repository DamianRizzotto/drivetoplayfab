﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class ObjectPool<T, P> where T : MonoBehaviour, IPoolObject<P> {

    private readonly List<T> items;
    private readonly Transform container;

    public ObjectPool(Transform container) {
        this.container = container;
        items = container.GetComponentsInChildren<T>(true).ToList();

        if (items.Count == 0) {
            Debug.LogError("ObjectPool has no base object");
        }
    }

    public void Set(List<P> list, bool sortItems = false) {
        int itemCount = items.Count;

        //Initialize items. If none available clone the first one
        int i = 0;
        for (; i < list.Count; i++) {
            if (i < itemCount) {
                T item = items[i];
                item.gameObject.SetActive(true);
                if (sortItems) {
                    item.transform.SetSiblingIndex(i);
                }
                item.Initialize(list[i]);
            } else {
                CreateItem(list[i]);
            }
        }

        //Hide leftover items
        for (; i < itemCount; i++) {
            items[i].gameObject.SetActive(false);
        }
    }

    public T Add(P content) {
        //Try to find an inactive object
        foreach (T item in items) {
            if (!item.gameObject.activeSelf) {
                item.gameObject.SetActive(true);
                item.transform.SetAsLastSibling();
                item.Initialize(content);
                return item;
            }
        }

        //If none available clone the first one
        return CreateItem(content);
    }

    private T CreateItem(P content) {
        GameObject go = Object.Instantiate(items[0].gameObject, container);
        go.transform.localScale = Vector3.one;
        T item = go.GetComponent<T>();
        item.Initialize(content);
        items.Add(item);
        return item;
    }

    public T Find(System.Func<T, bool> func) {
        foreach (T item in items) {
            if (func(item)) {
                return item;
            }
        }
        return null;
    }

    public List<T> GetItems() {
        return items;
    }

    public int GetActiveItemCount() {
        return items.Count(x => x.gameObject.activeSelf);
    }
}
