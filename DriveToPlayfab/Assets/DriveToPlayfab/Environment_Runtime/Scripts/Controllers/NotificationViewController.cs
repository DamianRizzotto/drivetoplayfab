using System;
using UnityEngine;
using TMPro;

public class NotificationViewController : Singleton<NotificationViewController> {

    public enum NotificationType
    {
        INFO,
        SUCCESS,
        WARNING,
        ERROR
    }
    
    [SerializeField] private GameObject _infoPrefab;
    [SerializeField] private GameObject _successPrefab;
    [SerializeField] private GameObject _warningPrefab;
    [SerializeField] private GameObject _errorPrefab;

    public void Show(NotificationType notificationType, string text) {
        gameObject.SetActive(true);

        GameObject go;
        switch (notificationType)
        {
            case NotificationType.INFO:
                go = _infoPrefab;
                break;
            case NotificationType.SUCCESS:
                go = _successPrefab;
                break;
            case NotificationType.WARNING:
                go = _warningPrefab;
                break;
            case NotificationType.ERROR:
                go = _errorPrefab;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(notificationType), notificationType, null);
        }

        GameObject newNotification = Instantiate(go, transform);
        newNotification.GetComponentInChildren<TextMeshProUGUI>(true).text = text;
        newNotification.transform.SetAsFirstSibling();
        newNotification.SetActive(true);
    }
}
