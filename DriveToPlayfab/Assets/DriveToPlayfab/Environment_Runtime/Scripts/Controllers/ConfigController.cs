﻿using System.Collections;
using System.Collections.Generic;
using _Scripts.Model;
using PlayFab.Json;
using UnityEngine;
using UnityEngine.Windows;

public class ConfigController : Singleton<ConfigController>
{
    private Config _config;
    public Config Config
    {
        get
        {
            if (_config == null)
            {
                string path = $"{ConfigDirectory}/{ConfigFileName}";
                if (Directory.Exists(ConfigDirectory) && File.Exists(path))
                {
                    string json = System.IO.File.ReadAllText(path);
                    _config = PlayFabSimpleJson.DeserializeObject<Config>(json);
                } else
                {
                    _config = new Config();
                }
            }
            return _config;
        }
    }

    private const string ConfigDirectory = "./";
    private const string ConfigFileName = "Configuration.json";
    
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
        GameObject go = new GameObject("ConfigController");
        go.AddComponent<ConfigController>();
    }
    
    public void Save()
    {
        string json = PlayFabSimpleJson.SerializeObject(Config);
        if (!Directory.Exists(ConfigDirectory))
        {
            Directory.CreateDirectory(ConfigDirectory);
        }
        string path = $"{ConfigDirectory}/{ConfigFileName}";
        System.IO.File.WriteAllText(path, json);
    }
}
