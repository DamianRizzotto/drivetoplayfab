﻿using _Scripts.Model;
using _Scripts.UI;
using UnityEngine;

public class PlayfabViewController : MonoBehaviour
{
    [SerializeField] private Transform _listContainer;
    [SerializeField] private EnvironmentSyncListItem _item;
        
    private void OnEnable()
    {
        _item.gameObject.SetActive(false);
        UpdateUI();
    }

    public void UpdateUI()
    {
        foreach (Transform child in _listContainer)
        {
            if (child.gameObject.activeSelf)
            {
                Destroy(child.gameObject);
            }
        }

        foreach (PlayfabEnvironmentConfig config in ConfigController.Instance.Config.PlayfabConfig.EnvironmentConfigs)
        {
            GameObject go = Instantiate(_item.gameObject, _listContainer);
            EnvironmentSyncListItem environment = go.GetComponent<EnvironmentSyncListItem>();
            environment.Initialize(config);
            go.SetActive(true);
        }
    }
}
