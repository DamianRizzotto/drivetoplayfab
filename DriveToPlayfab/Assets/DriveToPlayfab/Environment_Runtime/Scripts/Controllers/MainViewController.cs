﻿using System.IO;
using SFB;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MainViewController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _appName;
    [SerializeField] private Image _background;
    [SerializeField] private AspectRatioFitter _backgroundAspectRatioFitter;
    [SerializeField] private Image _icon;
    [SerializeField] private Image _miniIcon;
    [SerializeField] private CanvasScaler _canvasScaler;
    [SerializeField] private RectTransform _mainPanelRect;
    [Space]
    [SerializeField] private GameObject _maximizeIcon;
    [SerializeField] private GameObject  _minimizeIcon;

    private const float MinCanvasHeight = 850;
    private const float MaxCanvasHeight = 1050;
    private const string ReferenceResolutionFactorKey = "ReferenceResolutionFactor";
    private const string ImageStoragePath = ".";
    private const string ImageBackground = "Background";
    private const string ImageIcon = "Icon";
    private const string ImageMiniIcon = "MiniIcon";
    
    private void OnEnable()
    {
        UpdateUI();
        UpdateMargins();
    }

    private void UpdateMargins()
    {
        if (Screen.fullScreen)
        {
            _mainPanelRect.anchorMin = Vector2.zero;
            _mainPanelRect.anchorMax = Vector2.one;
            _mainPanelRect.sizeDelta = Vector2.zero;
            Screen.SetResolution(Screen.currentResolution.width / 2, Screen.currentResolution.height / 2, FullScreenMode.Windowed);
        } else
        {
            _mainPanelRect.anchorMin = Vector2.one * 0.05f;
            _mainPanelRect.anchorMax = Vector2.one * 0.95f;
            _mainPanelRect.sizeDelta = Vector2.zero;
            Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, FullScreenMode.ExclusiveFullScreen);
        }
    }

    private void UpdateUI()
    {
        _appName.text = ConfigController.Instance.Config.AppName;
        
        float referenceResolutionFactor = PlayerPrefs.GetFloat(ReferenceResolutionFactorKey, 0.5f);
        _canvasScaler.referenceResolution = new Vector2(0, Mathf.Lerp(MinCanvasHeight, MaxCanvasHeight, referenceResolutionFactor));

        LoadImage(ImageMiniIcon, _miniIcon);
        LoadImage(ImageIcon, _icon);
        Texture2D backgroundTex = LoadImage(ImageBackground, _background);
        if (backgroundTex != null)
        {
            _backgroundAspectRatioFitter.aspectRatio = backgroundTex.width / (float) backgroundTex.height;
        }
    }

    private Texture2D LoadImage(string imageFileName, Image image)
    {
        string path = $"{ImageStoragePath}/{imageFileName}.png";
        
        Texture2D tex = null;
        if (File.Exists(path))
        {
            byte[] fileData = File.ReadAllBytes(path);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
            image.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        }
        image.gameObject.SetActive(tex != null);
        return tex;
    }
    
    public void UpdateAppName(string appName)
    {
        _appName.text = appName;
    }

    public void UpdateCanvasReferenceResolution(float value)
    {
        value /= 10f;
        PlayerPrefs.SetFloat(ReferenceResolutionFactorKey, value);
        _canvasScaler.referenceResolution = new Vector2(0, Mathf.Lerp(MinCanvasHeight, MaxCanvasHeight, value));
    }

    public float GetReferenceResolutionFactor()
    {
         return Mathf.InverseLerp(MinCanvasHeight, MaxCanvasHeight, _canvasScaler.referenceResolution.y);
    }

    public void UpdateBackground()
    {
        Texture2D tex = UpdateImage(ImageBackground, _background);
        if (tex != null)
        {
            _backgroundAspectRatioFitter.aspectRatio = tex.width / (float) tex.height;
        }
    }

    public void UpdateMiniIcon()
    {
        UpdateImage(ImageMiniIcon, _miniIcon);
    }

    public void UpdateIcon()
    {
        UpdateImage(ImageIcon, _icon);
    }

    private Texture2D UpdateImage(string fileName, Image image)
    {
        // Open file with filter
        var extensions = new [] {
            new ExtensionFilter("Image Files", "png", "jpg", "jpeg" ),
        };
        string[] paths = StandaloneFileBrowser.OpenFilePanel("Open File", "", extensions, true);

        if (paths.Length == 0) return null;

        Texture2D tex = null;
        if (File.Exists(paths[0])){
            byte[] fileData = File.ReadAllBytes(paths[0]);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); 
            
            //Set sprite
            image.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

            //Save File
            byte[] pngBites = tex.EncodeToPNG();
            File.WriteAllBytes($"{ImageStoragePath}/{fileName}.png", pngBites);
        }

        image.gameObject.SetActive(tex != null);
        return tex;
    }
    
#region UI Events

    public void ButtonClick_Exit()
    {
        Application.Quit();
    }

    public void ButtonClick_ScreenMode()
    {
        Screen.fullScreen = !Screen.fullScreen;
        UpdateMargins();
        _minimizeIcon.SetActive(Screen.fullScreen);
        _maximizeIcon.SetActive(!Screen.fullScreen);
    }
    
#endregion

}
