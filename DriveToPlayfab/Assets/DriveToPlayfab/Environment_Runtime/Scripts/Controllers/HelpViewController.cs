﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;

public class HelpViewController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;
    
    private const string HelpDocFileID = "1DtXhKDEAgMgptOmAkGagc-wnjrLx23np6rGpvs7dulU";
    private const string PlayerPrefsKey = "HelpText";
        
    private void Start()
    {
        ShowHelpText();
        string url = $"https://docs.google.com/document/d/{HelpDocFileID}/export?format=txt";
        Timing.RunCoroutine(_DownloadRoutine(url));
    }

    private IEnumerator<float> _DownloadRoutine(string url)
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        www.SendWebRequest();
        
        yield return Timing.WaitUntilTrue( () => www.isDone);
        
        if (string.IsNullOrEmpty(www.error))
        {
            PlayerPrefs.SetString(PlayerPrefsKey, www.downloadHandler.text);
            PlayerPrefs.Save();
            ShowHelpText();
        } else
        {
            Debug.LogError($"Help file request failed {www.error} ");
        }
    }

    private void ShowHelpText()
    {
        string inputString = PlayerPrefs.GetString(PlayerPrefsKey, "Loading...");

        inputString = Encoding.ASCII.GetString(
            Encoding.Convert(Encoding.UTF8,Encoding.GetEncoding(
                    Encoding.ASCII.EncodingName,
                    new EncoderReplacementFallback(string.Empty),
                    new DecoderExceptionFallback()
                ), Encoding.UTF8.GetBytes(inputString)
            ));

        _text.text = inputString;
    }
}
