﻿using System.Collections;
using System.Collections.Generic;
using _Scripts.Model;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityGoogleDrive;

public class SettingsViewController : MonoBehaviour
{
    [SerializeField] private MainViewController _mainViewController;
    [SerializeField] private AlwaysBottomScroll _alwaysBottomScroll;
    
    [Header("General")]
    [SerializeField] private TMP_InputField _appNameInputField;
    [SerializeField] private Transform _fileProcessingList;
    [SerializeField] private Button _addFileProcessingConfigButton;

    [Header("Google Drive")]
    [SerializeField] private Button _logoutFromGoogleButton;
    [SerializeField] private TMP_InputField _folderNameOrIdInputField;
    [SerializeField] private Toggle _ignoreTrashedFilesToggle;

    [Header("Playfab")] 
    [SerializeField] private Transform _environmentList;
    [SerializeField] private Button _addEnvironmentConfigButton;
    
    [Header("Appearance")] 
    [SerializeField] private Slider _zoomSlider;
    [SerializeField] private Button _changeBackgroundImageButton;
    [SerializeField] private Button _changeIconImageButton;
    [SerializeField] private Button _changeMiniIconImageButton;
    
    private ObjectPool<FileProcessingListItem, FileProcessingConfig> _fileProcessingPool;
    private ObjectPool<EnvironmentListItem, PlayfabEnvironmentConfig> _environmentPool;
    private Config _config;

    private void Awake()
    {
        _config = ConfigController.Instance.Config;

        if (_fileProcessingPool == null)
        {
            _fileProcessingPool = new ObjectPool<FileProcessingListItem, FileProcessingConfig>(_fileProcessingList);
        }
        
        if (_environmentPool == null)
        {
            _environmentPool = new ObjectPool<EnvironmentListItem, PlayfabEnvironmentConfig>(_environmentList);
        }

        _appNameInputField.onValueChanged.AddListener(InputField_AppName);
        _addFileProcessingConfigButton.onClick.AddListener(ButtonClick_AddFileProcessingItem);
        _logoutFromGoogleButton.onClick.AddListener(ButtonClick_LogoutFromGoogle);
        _folderNameOrIdInputField.onValueChanged.AddListener(InputField_DriveFolderNameOrId);
        _ignoreTrashedFilesToggle.onValueChanged.AddListener(ToggleChange_IgnoreTrashedFiles);
        _addEnvironmentConfigButton.onClick.AddListener(ButtonClick_AddEnvironment);
        
        _zoomSlider.onValueChanged.AddListener(_mainViewController.UpdateCanvasReferenceResolution);
        _changeBackgroundImageButton.onClick.AddListener(_mainViewController.UpdateBackground);
        _changeIconImageButton.onClick.AddListener(_mainViewController.UpdateIcon);
        _changeMiniIconImageButton.onClick.AddListener(_mainViewController.UpdateMiniIcon);
    }
    
    private void OnEnable()
    {
        UpdateUI();
    }

    public void UpdateUI()
    {
        //General
        _appNameInputField.text = _config.AppName;
        _fileProcessingPool.Set(_config.FileProcessingConfigs);
        
        //Drive
        _logoutFromGoogleButton.transform.parent.gameObject.SetActive(GoogleDriveSettings.LoadFromResources().IsAnyAuthTokenCached());
        _folderNameOrIdInputField.text = _config.GoogleDriveConfig.FolderNameOrId;
        _ignoreTrashedFilesToggle.isOn = _config.GoogleDriveConfig.IgnoreTrashedFiles;

        //Playfab
        _environmentPool.Set(_config.PlayfabConfig.EnvironmentConfigs);
        
        //Appearance
        _zoomSlider.value = _mainViewController.GetReferenceResolutionFactor() * 10;
        
        Canvas.ForceUpdateCanvases();
        ((RectTransform) transform).ForceUpdateRectTransforms();
    }

    private void OnDisable()
    {
        if (ConfigController.Instance)
        {
            ConfigController.Instance.Save();
        }
    }

    private void OnDestroy()
    {
        if (ConfigController.Instance)
        {
            ConfigController.Instance.Save();
        }
    }

    #region UI Events

    //General

    private void InputField_AppName(string value)
    {
        _config.AppName = value;
        _mainViewController.UpdateAppName(value);
    }

    private void ButtonClick_AddFileProcessingItem()
    {
        _config.FileProcessingConfigs.Add(new FileProcessingConfig());
        UpdateUI();
    }
    
    //Drive

    private void ButtonClick_LogoutFromGoogle()
    { 
        GoogleDriveSettings.LoadFromResources().DeleteCachedAuthTokens();
        NotificationViewController.Instance.Show(NotificationViewController.NotificationType.SUCCESS,"Logged out from google");
        UpdateUI();
    }

    private void InputField_DriveFolderNameOrId(string value)
    {
        _config.GoogleDriveConfig.FolderNameOrId = value;
    }

    private void ToggleChange_IgnoreTrashedFiles(bool value)
    {
        _config.GoogleDriveConfig.IgnoreTrashedFiles = value;
    }
    
    //Playfab

    private void ButtonClick_AddEnvironment()
    {
        _config.PlayfabConfig.EnvironmentConfigs.Add(new PlayfabEnvironmentConfig());
        UpdateUI();
        _alwaysBottomScroll.enabled = true;
    }
    
    #endregion
}
