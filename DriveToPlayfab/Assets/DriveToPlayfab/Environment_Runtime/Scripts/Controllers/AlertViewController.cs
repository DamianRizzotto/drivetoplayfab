using System;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class AlertViewController : Singleton<AlertViewController> {

    [SerializeField] private GameObject _content;
    [SerializeField] private TextMeshProUGUI _titleLabel;
    [SerializeField] private TextMeshProUGUI _contentLabel;
    [Space]
    [SerializeField] private GameObject _buttonsContainer;
    [SerializeField] private Button _redButton;
    [SerializeField] private TextMeshProUGUI _redButtonLabel;
    [SerializeField] private Button _greenButton;
    [SerializeField] private TextMeshProUGUI _greenButtonLabel;
    [Space]
    [SerializeField] private GameObject _progressBarContainer;
    [SerializeField] private TextMeshProUGUI _progressBarLabel;
    [SerializeField] private ProgressBar _progressBar;

    public void Show(string title, bool showProgressBar) 
    {
        Show(title, string.Empty, string.Empty, null, string.Empty, null, showProgressBar);
    }
    
    public void Show(string title, string content, bool showProgressBar) 
    {
        Show(title, content, string.Empty, null, string.Empty, null, showProgressBar);
    }

    public void Show(string title, string content, string redButtonText, Action redButtonClicked, string greenButtonText, Action greenButtonClicked, bool showProgressBar = false) 
    {
        _content.SetActive(true);

        _titleLabel.text = title;
        _contentLabel.text = content;

        //Buttons
        if (string.IsNullOrEmpty(redButtonText))
        {
            _redButton.gameObject.SetActive(false);
        } else
        {
            _redButtonLabel.text = redButtonText;
            _redButton.gameObject.SetActive(true);
            _redButton.onClick.RemoveAllListeners();
            _redButton.onClick.AddListener(()=> redButtonClicked());
        }
        
        if (string.IsNullOrEmpty(greenButtonText))
        {
            _greenButton.gameObject.SetActive(false);
        } else
        {
            _greenButtonLabel.text = greenButtonText;
            _greenButton.gameObject.SetActive(true);
            _greenButton.onClick.RemoveAllListeners();
            _greenButton.onClick.AddListener(()=> greenButtonClicked());
        }
        
        _buttonsContainer.SetActive(_redButton.gameObject.activeSelf || _greenButton.gameObject.activeSelf);
        _progressBarContainer.SetActive(showProgressBar);
    }

    public void ModifyProgressBar(string text, float value) 
    {
        _progressBarContainer.SetActive(true);
        _progressBarLabel.text = text;
        _progressBar.SetProgress(value);
    }

    public void Close() 
    {
        _content.SetActive(false);
    }
}
