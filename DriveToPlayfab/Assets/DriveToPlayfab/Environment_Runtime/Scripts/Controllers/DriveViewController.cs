﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _Scripts.Model;
using TMPro;
using UnityEditorInternal.VersionControl;
using UnityEngine;
using UnityEngine.UI;
using UnityGoogleDrive;
using UnityGoogleDrive.Data;

public class DriveViewController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _lastUpdatedLabel;
    [Space]
    [SerializeField] private Toggle _hierarchyToggle;
    [SerializeField] private Toggle _lastModifiedToggle;
    [Space]
    [SerializeField] private Transform _listContainer;
    [SerializeField] private FileListItem _folder;
    [SerializeField] private FileListItem _file;

    private const string ListModePlayerPrefsKey = "ListMode";
    
    private void OnEnable()
    {
        bool hierarchyMode = PlayerPrefs.GetInt(ListModePlayerPrefsKey, 0) == 0;
        _hierarchyToggle.isOn = hierarchyMode;
        _lastModifiedToggle.isOn = !hierarchyMode;
        
        _folder.gameObject.SetActive(false);
        _file.gameObject.SetActive(false);
        
        UpdateUI();
    }
    
    private void UpdateUI()
    {
        GoogleDriveConfig config = ConfigController.Instance.Config.GoogleDriveConfig;
        File folder = config.Folder;
        
        if (folder == null) return;

        if (config.LastUpdated != null)
        {
            _lastUpdatedLabel.text = $"Last Updated: {config.LastUpdated.Value.ToLocalTime():MMMM dd hh:mm tt}";
        }

        foreach (Transform child in _listContainer)
        {
            if (child.gameObject.activeSelf)
            {
                Destroy(child.gameObject);
            }
        }

        if (_hierarchyToggle.isOn)
        {
            AddFolderUI(config.FileList, folder, _listContainer);
        } else
        {
            var filteredList = new List<File>();
            AddLastModifiedUi(config.FileList, folder, ref filteredList, _listContainer);
        }
    }

    private void AddFolderUI(List<File> files, File folder, Transform parent)
    {
        GameObject folderGameObject = Instantiate(_folder.gameObject, parent);
        FileListItem folderItem = folderGameObject.GetComponent<FileListItem>();
        folderItem.Initialize(folder);
        folderGameObject.SetActive(true);
        
        var resources = files.Where(f => f.Parents != null && f.Parents.Contains(folder.Id));

        foreach (File resource in resources)
        {
            switch (resource.MimeType)
            {
                case Helpers.FolderMimeType:
                    AddFolderUI(files, resource, folderItem.SubfileContainer);
                    break;
                default:
                    AddFile(resource, folderItem.SubfileContainer);
                    break;
            }
        }
    }

    private void AddLastModifiedUi(List<File> files, File folder, ref List<File> filteredList, Transform parent = null)
    {
        var resources = files.Where(f => f.Parents != null && f.Parents.Contains(folder.Id));

        foreach (File resource in resources)
        {
            switch (resource.MimeType)
            {
                case Helpers.FolderMimeType:
                    AddLastModifiedUi(files, resource, ref filteredList);
                    break;
                default:
                    filteredList.Add(resource);
                    break;
            }
        }

        if (parent != null)
        {
            filteredList = filteredList.OrderByDescending(f => f.ModifiedTime ?? DateTime.MinValue).ToList();
            foreach (File file in filteredList)
            {
                AddFile(file, parent);
            }
        }
    }
    
    private void AddFile(File file, Transform parent)
    {
        GameObject fileGo = Instantiate(_file.gameObject, parent);
        FileListItem fileListItem = fileGo.GetComponent<FileListItem>();
        fileListItem.Initialize(file);
        fileGo.SetActive(true);
    }

    public List<File> GetMarkedFilesForUpload()
    {
        return _listContainer.GetComponentsInChildren<FileListItem>(true)
            .Where(fli => fli.File != null && fli.File.MimeType != Helpers.FolderMimeType && fli.Toggle.isOn)
            .Select(fli => fli.File).ToList();
    }
    
    #region UI Events

    public void ToggleChange()
    {
        PlayerPrefs.SetInt(ListModePlayerPrefsKey, _hierarchyToggle.isOn ? 0 : 1);
        PlayerPrefs.Save();
        
        UpdateUI();
    }

    public void ButtonClick_DownloadFiles()
    {
        DriveService.DownloadDrive(UpdateUI);
    }
    
    #endregion
}
