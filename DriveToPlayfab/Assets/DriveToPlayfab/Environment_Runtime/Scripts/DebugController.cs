﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugController : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKey(KeyCode.F1))
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
            NotificationViewController.Instance.Show(NotificationViewController.NotificationType.SUCCESS, "Deleted player prefs");
        }
    }
}
