﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityGoogleDrive;
using UnityGoogleDrive.Data;

public class FileListItem : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _nameLabel;
    [SerializeField] private Image _sideImage;
    [SerializeField] private Toggle _toggle;
    
    [Header("Folder")]
    [SerializeField] private Transform _subfileContainer;
    public Transform SubfileContainer => _subfileContainer;
    
    [Header("File")]
    [SerializeField] private Image _icon;
    [SerializeField] private Sprite[] _sprites;
    
    public File File { get; private set; }
    public Toggle Toggle => _toggle;

    private static readonly Color[] SideColors = {
        ColorManager.Blue,
        ColorManager.Purple,
        ColorManager.Green,
        ColorManager.Orange,
        ColorManager.Red,
    };

    public void Initialize(File file)
    {
        File = file;
        _nameLabel.text = file.Name;

        int parentFolders = GetComponentsInParent<FileListItem>().Length;
        _sideImage.color = parentFolders <= 0 ? Color.clear : SideColors[(parentFolders - 1) % SideColors.Length];
            
        if (_icon != null)
        {
            switch (File.MimeType)
            {
                case Helpers.SheetMimeType:
                    _icon.sprite = _sprites[0];
                    break;
                case Helpers.DocumentMimeType:
                    _icon.sprite = _sprites[1];
                    break;
                case Helpers.SlidesMimeType:
                    _icon.sprite = _sprites[2];
                    break;
                case "application/json":
                    _icon.sprite = _sprites[3];
                    break;
                case "text/csv":
                    _icon.sprite = _sprites[4];
                    break;
                case "application/pdf":
                    _icon.sprite = _sprites[5];
                    break;
                case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                    _icon.sprite = _sprites[6];
                    break;
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    _icon.sprite = _sprites[7];
                    break;
                case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
                    _icon.sprite = _sprites[8];
                    break;
            }
        }
    }

    private void RefreshToggleStatus()
    {
        _toggle.isOn = GetComponentsInChildren<FileListItem>(true).All(fli => fli.Toggle.isOn || fli == this);
    }
    
    #region UI Events

    public void ButtonClick_Info()
    {
        string fileData = $"Last Modified: {File.ModifiedTime?.ToString("MMMM dd hh:mm tt")} \n" +
                          $"Last Modified by: {File.LastModifyingUser?.DisplayName} \n" +
                          $"Trashed: {File.Trashed.HasValue && File.Trashed.Value}  \n" +
                          $"Created Time: {File.CreatedTime} \n" +
                          $"MimeType: {File.MimeType}";
        
        void OnOpen()
        {
            switch (File.MimeType)
            {
                case Helpers.SheetMimeType:
                    Application.OpenURL($"https://docs.google.com/spreadsheets/d/{File.Id}");
                    break;
                case Helpers.DocumentMimeType:
                    Application.OpenURL($"https://docs.google.com/document/d/{File.Id}");
                    break;
                case Helpers.SlidesMimeType:
                    Application.OpenURL($"https://docs.google.com/presentation/d/{File.Id}");
                    break;
                default:
                    Application.OpenURL($"https://drive.google.com/drive/u/1/folders/{File.Parents[0]}");
                    break;
            }
        }

        AlertViewController.Instance.Show(File.Name, fileData, "Close", AlertViewController.Instance.Close, "Open in browser", OnOpen);
    }

    public void ToggleChange_Selection()
    {
        Timing.RunCoroutine(_UpdateToggles());
    }

    #endregion

    private IEnumerator<float> _UpdateToggles()
    {
        yield return 0;

        if (File.MimeType == Helpers.FolderMimeType)
        {
            foreach (FileListItem item in _subfileContainer.GetComponentsInChildren<FileListItem>(true))
            {
                item.Toggle.isOn = _toggle.isOn;
            }
        }
        
        yield return 0;

        foreach (FileListItem item in GetComponentsInParent<FileListItem>())
        {
            if (item.File.MimeType == Helpers.FolderMimeType)
            {
                item.RefreshToggleStatus();
                yield return 0;
            }
        }
    }
}
