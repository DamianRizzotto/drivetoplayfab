﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Scripts.Model;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityGoogleDrive;

public class FileProcessingListItem : MonoBehaviour, IPoolObject<FileProcessingConfig>
{
    [SerializeField] private SettingsViewController _settingsViewController;
    [Space]
    [SerializeField] private TMP_InputField _inputMimeTypeInputField;
    [SerializeField] private TMP_InputField _outputMimeTypeInputField;
    [SerializeField] private TMP_InputField _extensionInputField;
    [SerializeField] private Button _removeButton;

    private FileProcessingConfig InitData;

    private void Awake()
    {
        _inputMimeTypeInputField.onValueChanged.AddListener(InputField_InputMimeType);
        _outputMimeTypeInputField.onValueChanged.AddListener(InputField_outputMimeType);
        _extensionInputField.onValueChanged.AddListener(InputField_Extension);
        _removeButton.onClick.AddListener(ButtonClick_Remove);
        
    }

    public void Initialize(FileProcessingConfig initData)
    {
        InitData = initData;
        
        bool googleFile = initData.InputMimeType == Helpers.SheetMimeType ||
                                initData.InputMimeType == Helpers.DocumentMimeType ||
                                initData.InputMimeType == Helpers.SlidesMimeType;
        _removeButton.gameObject.SetActive(!googleFile);

        _inputMimeTypeInputField.interactable = !googleFile;
        _inputMimeTypeInputField.text = initData.InputMimeType;
        _outputMimeTypeInputField.text = initData.OutputMimeType;
        _extensionInputField.text = initData.Extension;

    }

    #region UI Events

    private void InputField_InputMimeType(string value)
    {
        InitData.InputMimeType = value;
    }

    private void InputField_outputMimeType(string value)
    {
        InitData.OutputMimeType = value;
    }

    private void InputField_Extension(string value)
    {
        InitData.Extension = value;
    }

    private void ButtonClick_Remove()
    {
        ConfigController.Instance.Config.FileProcessingConfigs.Remove(InitData);
        _settingsViewController.UpdateUI();
    }
    
    #endregion
}
