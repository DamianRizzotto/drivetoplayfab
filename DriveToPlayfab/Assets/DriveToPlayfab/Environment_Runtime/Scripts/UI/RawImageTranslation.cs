﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class RawImageTranslation : MonoBehaviour
{
    [SerializeField] private float xSpeed;
    [SerializeField] private float ySpeed;
    
    private RawImage _rawImage;

    private void Awake()
    {
        _rawImage = GetComponent<RawImage>();
    }

    private void Update()
    {
        Rect rect = _rawImage.uvRect;
        rect.x += xSpeed * Time.deltaTime;
        rect.y += ySpeed * Time.deltaTime;
        _rawImage.uvRect = rect;
    }
}
