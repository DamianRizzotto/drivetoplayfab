﻿using System.Collections.Generic;
using _Scripts.Model;
using TMPro;
using UnityEngine;
using UnityGoogleDrive.Data;

namespace _Scripts.UI {
    public class EnvironmentSyncListItem : MonoBehaviour
    {
        [SerializeField] private DriveViewController _driveViewController;
        [SerializeField] private PlayfabViewController _playfabViewController;
        [SerializeField] private TextMeshProUGUI _name;
        [SerializeField] private TextMeshProUGUI _lastUpdated;

        private PlayfabEnvironmentConfig Config { get; set; }
    
        public void Initialize(PlayfabEnvironmentConfig config)
        {
            Config = config;
            _name.text = config.Name;

            if (config.LastSync == null)
            {
                _lastUpdated.text = "Never";
                _lastUpdated.color = ColorManager.Red;
            } else
            {
                _lastUpdated.text = config.LastSync.Value.ToString("MMMM dd hh:mm tt");
                _lastUpdated.color = ColorManager.Green;
            }
        }

        #region UI Events

        public void ButtonClick_Sync()
        {
            //Get folder
            File folder = ConfigController.Instance.Config.GoogleDriveConfig.Folder;

            if (folder == null)
            {
                NotificationViewController.Instance.Show(NotificationViewController.NotificationType.ERROR, $"Folder not found");
                return;
            }
        
            List<File> filesMarkedForUpload = _driveViewController.GetMarkedFilesForUpload();
            
            //Update file contents
            DriveService.UpdateFileContents( () =>
            {
                //Upload files to this environment
                PlayfabService.UploadFilesUnderFolder(folder, DriveService.GetFiles(), Config, () =>
                {
                    _playfabViewController.UpdateUI();
                }, filesMarkedForUpload);
            }, filesMarkedForUpload);
        }

        #endregion
    }
}
