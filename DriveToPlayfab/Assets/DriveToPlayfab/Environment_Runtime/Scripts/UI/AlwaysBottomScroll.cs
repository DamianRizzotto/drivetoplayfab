﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(ScrollRect))]
public class AlwaysBottomScroll : MonoBehaviour, IBeginDragHandler, IEndDragHandler {

    [SerializeField] private bool _disableOnArrival = true;
    [SerializeField] private float _speed = 1;

    private ScrollRect _rect;
    private bool _dragging;

    private void Awake() {
        _rect = GetComponent<ScrollRect>();
    }

    public void OnBeginDrag(PointerEventData eventData) {
        _dragging = true;
    }

    public void OnEndDrag(PointerEventData eventData) {
        _dragging = false;
    }

    private void Update() {
        if (_dragging) return;

        if (_rect.verticalNormalizedPosition > 0.005f) {
            _rect.verticalNormalizedPosition = Mathf.Lerp(_rect.verticalNormalizedPosition, 0, Time.deltaTime * _speed);
        } else if (_disableOnArrival) {
            enabled = false;
        }
    }
}
