﻿using UnityEngine;

public class ProgressBar : MonoBehaviour
{
	private RectTransform _rectTransform;
	private RectTransform _parentRectTransform;

	private float _targetWidth = 0;

	private void OnEnable()
	{
		if (_rectTransform == null)
		{
			_rectTransform = (RectTransform) transform;
			_parentRectTransform = (RectTransform) transform.parent;
		}
		_rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 0);
	}
	
	public void SetProgress(float value)
	{
		value = Mathf.Clamp01(value);
		
		if (_rectTransform == null)
		{
			_rectTransform = (RectTransform) transform;
			_parentRectTransform = (RectTransform) transform.parent;
		}

		_targetWidth = _parentRectTransform.rect.width * value;
	}

	private void Update()
	{
		_rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Lerp(_rectTransform.rect.width, _targetWidth, Time.deltaTime));
	}
}
