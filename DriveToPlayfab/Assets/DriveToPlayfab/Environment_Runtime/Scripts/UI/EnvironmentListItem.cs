﻿using _Scripts.Model;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EnvironmentListItem : MonoBehaviour, IPoolObject<PlayfabEnvironmentConfig>
{
    [SerializeField] private SettingsViewController _settingsViewController;
    [Space]
    [SerializeField] private TMP_InputField _nameInputField;
    [SerializeField] private TMP_InputField _titleIdInputField;
    [SerializeField] private TMP_InputField _developerSecretKeyInputField;
    [SerializeField] private Button _removeButton;

    private PlayfabEnvironmentConfig InitData;

    private void Awake()
    {
        _nameInputField.onValueChanged.AddListener(InputField_Name);
        _titleIdInputField.onValueChanged.AddListener(InputField_TitleID);
        _developerSecretKeyInputField.onValueChanged.AddListener(InputField_DeveloperSecretKeyInputField);
        _removeButton.onClick.AddListener(ButtonClick_Remove);
    }

    public void Initialize(PlayfabEnvironmentConfig initData)
    {
        InitData = initData;
        
        _removeButton.gameObject.SetActive(transform.GetSiblingIndex() != 0);
        
        _nameInputField.text = initData.Name;
        _titleIdInputField.text = initData.TitleId;
        _developerSecretKeyInputField.text = initData.DeveloperSecretKey;
    }

    #region UI Events

    private void InputField_Name(string value)
    {
        InitData.Name = value;
        InitData.LastSync = null;
    }

    private void InputField_TitleID(string value)
    {
        InitData.TitleId = value;
    }

    private void InputField_DeveloperSecretKeyInputField(string value)
    {
        InitData.DeveloperSecretKey = value;
    }

    private void ButtonClick_Remove()
    {
        ConfigController.Instance.Config.PlayfabConfig.EnvironmentConfigs.Remove(InitData);
        _settingsViewController.UpdateUI();
    }
    
    #endregion
}
