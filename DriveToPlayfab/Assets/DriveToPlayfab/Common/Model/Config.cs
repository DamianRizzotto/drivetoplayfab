﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityGoogleDrive;
using UnityGoogleDrive.Data;

namespace _Scripts.Model {
    
    [Serializable]
    public class Config
    {
        public string AppName;
        public bool ConvertFileNamesToLower = true;
        public bool ReplaceEmptySpacesForUnderscores = true;
        public List<FileProcessingConfig> FileProcessingConfigs = new List<FileProcessingConfig>();
        
        [Space]
        [Header("Google Drive")]
        public GoogleDriveConfig GoogleDriveConfig = new GoogleDriveConfig();
        
        [Space]
        [Header("Playfab")]
        public PlayfabConfig PlayfabConfig = new PlayfabConfig();
        
        [Space]
        [Header("Local Backup")]
        public LocalBackupConfig LocalBackupConfig = new LocalBackupConfig();
    
        public Config()
        {
            PlayfabEnvironmentConfig prodConfig = new PlayfabEnvironmentConfig()
            {
                Name = "Production",
            };
            PlayfabConfig.EnvironmentConfigs.Add(prodConfig);
        
            FileProcessingConfig fileProcessingConfig = new FileProcessingConfig()
            {
                InputMimeType = Helpers.SheetMimeType,
                OutputMimeType = "text/csv",
                Extension = "csv",
            };
            FileProcessingConfigs.Add(fileProcessingConfig);
        
            fileProcessingConfig = new FileProcessingConfig()
            {
                InputMimeType = Helpers.DocumentMimeType,
                OutputMimeType = "text/plain",
                Extension = "json",
            };
            FileProcessingConfigs.Add(fileProcessingConfig);
        
            fileProcessingConfig = new FileProcessingConfig()
            {
                InputMimeType = Helpers.SlidesMimeType,
                OutputMimeType = "application/pdf",
                Extension = "pdf",
            };
            FileProcessingConfigs.Add(fileProcessingConfig);
        
            fileProcessingConfig = new FileProcessingConfig()
            {
                InputMimeType = "application/json",
                OutputMimeType = "text/plain",
                Extension = "json",
            };
            FileProcessingConfigs.Add(fileProcessingConfig);
        
            fileProcessingConfig = new FileProcessingConfig()
            {
                InputMimeType = "text/csv",
                OutputMimeType = "text/plain",
                Extension = "csv",
            };
            FileProcessingConfigs.Add(fileProcessingConfig);

            GoogleDriveConfig.IgnoreTrashedFiles = true;
        }
    }

    [Serializable]
    public class FileProcessingConfig
    {
        public string InputMimeType;
        public string OutputMimeType;
        public string Extension;
    }

    [Serializable]
    public class GoogleDriveConfig
    {
        public string FolderNameOrId;
        public DateTime? LastUpdated;
        public List<File> FileList = new List<File>();
        public File Folder;
        public bool IgnoreTrashedFiles;
    }

    [Serializable]
    public class PlayfabConfig
    {
        public List<PlayfabEnvironmentConfig> EnvironmentConfigs = new List<PlayfabEnvironmentConfig>();
    }

    [Serializable]
    public class PlayfabEnvironmentConfig
    {
        public string Name;
        public DateTime? LastSync;
        public string TitleId;
        public string DeveloperSecretKey;
    }

    [Serializable]
    public class LocalBackupConfig
    {
        public string BackupPath;
        public string SubFolderNameOrId;
    }
}