﻿using System.Collections.Generic;
using _Scripts.Model;
using MEC;
using UnityEditor;
using UnityEngine;

public static class ServiceHelper
{
    private static string lastMessage;
    
    public static Config GetConfig()
    {
        if (Application.isPlaying)
        {
            return ConfigController.Instance.Config;
        }
        return DriveToPlayfab.Instance.Config;
    }

    public static void Save()
    {
        if (Application.isPlaying)
        {
            ConfigController.Instance.Save();
        } else
        {
            EditorUtility.SetDirty(DriveToPlayfab.Instance);
        }
    }

    public static CoroutineHandle RunCoroutine(IEnumerator<float> coroutine)
    {
        if (Application.isPlaying)
        {
            return Timing.RunCoroutine(coroutine);
        } 
        return Timing.RunCoroutine(coroutine, Segment.EditorUpdate);
    }
    
    public static void ShowMessage(string title, bool showProgressBar) 
    {
        if (Application.isPlaying)
        {
            AlertViewController.Instance.Show(title, showProgressBar);
        } else
        {
            lastMessage = title;
        }
    }

    public static void ShowMessage(string title, string content, bool showProgressBar) 
    {
        if (Application.isPlaying)
        {
            AlertViewController.Instance.Show(title, content, showProgressBar);
        } else
        {
            lastMessage = title;
        }
    }

    public static void ShowProgressBar(string text, float progress)
    {
        if (Application.isPlaying)
        {
            AlertViewController.Instance.ModifyProgressBar(text, progress);
        } else
        {
            EditorUtility.DisplayProgressBar(lastMessage, text, progress);
        }
    }

    public static void HideMessage()
    {
        if (Application.isPlaying)
        {
            AlertViewController.Instance.Close();
        } else
        {
            EditorUtility.ClearProgressBar();
        }
    }

    public static void ShowInfoNotification(string msg)
    {
        if (Application.isPlaying)
        {
            NotificationViewController.Instance.Show(NotificationViewController.NotificationType.INFO, msg);
        } else
        {
            Debug.Log(msg);
        }
    }

    public static void ShowErrorNotification(string msg)
    {
        if (Application.isPlaying)
        {
            NotificationViewController.Instance.Show(NotificationViewController.NotificationType.ERROR, msg);
        } else
        {
            Debug.LogError(msg);
        }
    }

    public static void ShowWarningNotification(string msg)
    {
        if (Application.isPlaying)
        {
            NotificationViewController.Instance.Show(NotificationViewController.NotificationType.WARNING, msg);
        } else
        {
            Debug.LogWarning(msg);
        }
    }

    public static void ShowSuccessNotification(string msg)
    {
        if (Application.isPlaying)
        {
            NotificationViewController.Instance.Show(NotificationViewController.NotificationType.SUCCESS, msg);
        } else
        {
            Debug.Log(msg);
        }
    }
}
