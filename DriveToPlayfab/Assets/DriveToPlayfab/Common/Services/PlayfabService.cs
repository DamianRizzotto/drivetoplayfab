﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using _Scripts.Model;
using MEC;
using PlayFab;
using PlayFab.AdminModels;
using PlayFab.Internal;
using UnityGoogleDrive;
using File = UnityGoogleDrive.Data.File;

public static class PlayfabService
{

    private const int UploadBatchSize = 15;

    public static void UploadFilesUnderFolder(File folder, List<File> files, PlayfabEnvironmentConfig config, Action onFinish, List<File> markedForUploadList = null, string path = "", List<IEnumerator<float>> coroutineList = null)
    {
        if(string.IsNullOrEmpty(config.TitleId) || string.IsNullOrEmpty(config.DeveloperSecretKey))
        {
            ServiceHelper.ShowErrorNotification("Environment config is missing values");
            return;
        }
        
        if (coroutineList == null)
        {
            coroutineList = new List<IEnumerator<float>>();
        }
        
        var resources = files.Where(f => f.Parents != null && f.Parents.Contains(folder.Id));

        foreach (File resource in resources)
        {
            if (resource.MimeType == Helpers.FolderMimeType)
            {
                UploadFilesUnderFolder(resource, files, config, null, markedForUploadList, string.IsNullOrEmpty(path) ? resource.Name : $"{path}/{CleanPathComponent(resource.Name)}", coroutineList);
            } else
            {
                
                if (markedForUploadList != null && !markedForUploadList.Contains(resource))
                {
                    continue;
                }
                
                FileProcessingConfig c = ServiceHelper.GetConfig().FileProcessingConfigs.FirstOrDefault(pfc => pfc.InputMimeType == resource.MimeType);
                if (c == null)
                {
                    ServiceHelper.ShowWarningNotification($"Did not found output MimeType for [{resource.Name}]");
                } else if (resource.Content == null)
                {
                    ServiceHelper.ShowWarningNotification($"File [{resource.Name}] does not have content");
                } else
                {
                    coroutineList.Add(_PutFile(path, resource, c));
                }
            }
        }

        if (onFinish != null)
        {
            ServiceHelper.RunCoroutine(_UploadFiles(coroutineList, config, onFinish));
        }
    }
    
    private static IEnumerator<float> _UploadFiles(List<IEnumerator<float>> coroutineList, PlayfabEnvironmentConfig config, Action onDownloadFinished)
    {
        PlayFabSettings.TitleId = config.TitleId;
        PlayFabSettings.DeveloperSecretKey = config.DeveloperSecretKey;
        
        int initialCoroutines = coroutineList.Count;
        int downloadedFiles = 0;
        
        ServiceHelper.ShowMessage("Uploading files to Playfab", true);
        
        while (coroutineList.Count > 0)
        {
            List<IEnumerator<float>> runningCoroutines = coroutineList.GetRange(0, Math.Min(coroutineList.Count, UploadBatchSize)).ToList();
            
            ServiceHelper.ShowProgressBar($"Files {downloadedFiles}-{downloadedFiles + runningCoroutines.Count} of {initialCoroutines}", downloadedFiles / (float) initialCoroutines);

            //Start coroutine
            List<CoroutineHandle> runningHandles = new List<CoroutineHandle>();
            foreach (IEnumerator<float> coroutine in runningCoroutines)
            {
                CoroutineHandle handle = ServiceHelper.RunCoroutine(coroutine);
                runningHandles.Add(handle);
            }
            
            //Wait for them to finish
            while (runningHandles.Exists(ch => ch.IsRunning))
            {
                yield return 0;
            }

            downloadedFiles += runningCoroutines.Count;
            
            //Remove them from the main list
            foreach (IEnumerator<float> coroutine in runningCoroutines)
            {
                coroutineList.Remove(coroutine);
            }
        }
        
        ServiceHelper.HideMessage();
        ServiceHelper.ShowSuccessNotification("Files successfully uploaded to Playfab");
        
        config.LastSync = DateTime.UtcNow;
        ServiceHelper.Save();
        
        onDownloadFinished?.Invoke();
    }
    
    private static IEnumerator<float> _PutFile(string filePath, File file, FileProcessingConfig config)
    {
        string fileName = CleanPathComponent(file.Name);

        if (ServiceHelper.GetConfig().ConvertFileNamesToLower)
        {
            fileName = fileName.ToLower();
        }

        if (ServiceHelper.GetConfig().ReplaceEmptySpacesForUnderscores)
        {
            fileName = fileName.Replace(" ", "_");
        }
        
        //Add file information to the full path
        filePath = $"{filePath}/{fileName}.{config.Extension}";
        
        string uploadUrl = string.Empty;
        bool waitingForPlayfabResponse = true;
        
        GetContentUploadUrlRequest getContentRequest = new GetContentUploadUrlRequest {ContentType = config.OutputMimeType, Key = filePath};
        PlayFabAdminAPI.GetContentUploadUrl(getContentRequest, result =>
        {
            uploadUrl = result.URL;
            waitingForPlayfabResponse = false;
        }, error =>
        {
            waitingForPlayfabResponse = false;
            ServiceHelper.ShowErrorNotification($"Did not get a upload URL from Playfab for [{filePath}]");
        });

        while (waitingForPlayfabResponse)
        {
            yield return 0;
        }

        if (string.IsNullOrEmpty(uploadUrl))
        {
            yield break;
        }
        
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uploadUrl);
        request.Method = "PUT";
        request.ContentType = config.OutputMimeType;
        
        Stream dataStream = request.GetRequestStream();
        dataStream.Write(file.Content, 0, file.Content.Length);
        dataStream.Close();

        void AsyncCallback(IAsyncResult asyncResult){ }
        HttpRequestState state = new HttpRequestState();
        IAsyncResult res = request.BeginGetResponse(AsyncCallback, state);

        while (!res.IsCompleted)
        {
            yield return 0;
        }

        if (state == HttpRequestState.Error)
        {
            ServiceHelper.ShowErrorNotification($"Upload failed for [{filePath}]");
        }
    }

    private static string CleanPathComponent(string pathComponent)
    {
        return string.Join(string.Empty, pathComponent.Split(Path.GetInvalidFileNameChars()));
    }
}
