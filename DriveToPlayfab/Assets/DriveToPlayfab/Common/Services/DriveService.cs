﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Scripts.Model;
using MEC;
using UnityEngine;
using UnityGoogleDrive;
using UnityGoogleDrive.Data;

public static class DriveService
{
    private const int DownloadBatchSize = 15;

    public static List<File> GetFiles()
    {
        return ServiceHelper.GetConfig().GoogleDriveConfig.FileList;
    }
    
    public static void DownloadDrive(Action OnFileListUpdated)
    {
        DownloadFiles(OnFileListUpdated);
    }
    
    private static void DownloadFiles(Action OnFinish, List<File> files = null, string pageToken = null, int page = 1)
    {
        if (files == null)
        {
            files = new List<File>();
        }
        
        ServiceHelper.ShowMessage("Downloading Google Drive file pages", true);
        ServiceHelper.ShowProgressBar($"Page: {page}", page / 10f);
        
        //Additional data about Spaces and Corpora
        //https://developers.google.com/drive/api/v3/about-files
        var fileRequest = GoogleDriveFiles.List();
        fileRequest.Spaces = "drive";
        fileRequest.Corpora = "user";
        fileRequest.PageToken = pageToken;
        fileRequest.Fields = new List<string>() {"*"};
        fileRequest.Send().OnDone += fileList =>
        {
            files.AddRange(fileList.Files);
            if (!string.IsNullOrEmpty(fileList.NextPageToken))
            {
                DownloadFiles(OnFinish, files, fileList.NextPageToken, page + 1);
            } else
            {
                ServiceHelper.HideMessage();
                if (CleanAndUpdateFolderList(files))
                {
                    ServiceHelper.ShowSuccessNotification($"Google Drive files updated");
                }
                OnFinish();
            }
        };
    }

    private static bool CleanAndUpdateFolderList(List<File> files)
    {
        GoogleDriveConfig googleDriveConfig = ServiceHelper.GetConfig().GoogleDriveConfig;
        
        //Search for the main folder
        List<File> mainFolder = files.Where(f =>
            f.MimeType == Helpers.FolderMimeType &&
            (f.Name == googleDriveConfig.FolderNameOrId || f.Id == googleDriveConfig.FolderNameOrId)).ToList();

        if (mainFolder.Count == 0)
        {
            ServiceHelper.ShowErrorNotification($"Folder [{googleDriveConfig.FolderNameOrId}] not found");
            return false;
        }
        
        if (mainFolder.Count > 1)
        {
            ServiceHelper.ShowWarningNotification($"There are {mainFolder.Count} folders named [{googleDriveConfig.FolderNameOrId}]");
            return false;
        }
        
        File folder = mainFolder[0];

        //Cleanup
        List<File> purgedList = new List<File>();

        List<string> parents = new List<string>() {folder.Id};
        while (parents.Count > 0)
        {
            List<File> childFiles = files.Where(f => f.Parents != null && f.Parents.Any(x => parents.Any(y => y == x))).ToList();
            purgedList.AddRange(childFiles);
            parents = childFiles.Where(f => f.MimeType == Helpers.FolderMimeType).Select(f => f.Id).ToList();
        }
        
        if (googleDriveConfig.IgnoreTrashedFiles)
        {
            purgedList = purgedList.Where(f => !f.Trashed.HasValue || f.Trashed.Value == false).ToList();
        }
        
        googleDriveConfig.FileList = purgedList;
        googleDriveConfig.Folder = folder;
        googleDriveConfig.LastUpdated = DateTime.UtcNow;
        ServiceHelper.Save();
        return true;
    }

    public static void GetFilesUnderFolderId(string folderNameOrId, Action<KeyValuePair<File,List<File>>> callback)
    {
        GoogleDriveConfig googleDriveConfig = ServiceHelper.GetConfig().GoogleDriveConfig;

        if (string.IsNullOrEmpty(folderNameOrId))
        {
            callback(new KeyValuePair<File, List<File>>(googleDriveConfig.Folder, googleDriveConfig.FileList));
            return;
        }
        
        File folder = googleDriveConfig.FileList.FirstOrDefault(f => f.Name == folderNameOrId || f.Id == folderNameOrId);

        if (folder == null)
        {
            ServiceHelper.ShowErrorNotification($"Could not find {folderNameOrId} under current file list");
            return;
        }
        
        if (folder.MimeType != Helpers.FolderMimeType)
        {
            ServiceHelper.ShowErrorNotification($"File {folderNameOrId} is not a folder");
            return;
        }
        
        //Cleanup
        List<File> purgedList = new List<File>();

        List<string> parents = new List<string>() {folder.Id};
        while (parents.Count > 0)
        {
            List<File> childFiles = googleDriveConfig.FileList.Where(f => f.Parents != null && f.Parents.Any(x => parents.Any(y => y == x))).ToList();
            purgedList.AddRange(childFiles);
            parents = childFiles.Where(f => f.MimeType == Helpers.FolderMimeType).Select(f => f.Id).ToList();
        }
        
        if (googleDriveConfig.IgnoreTrashedFiles)
        {
            purgedList = purgedList.Where(f => !f.Trashed.HasValue || f.Trashed.Value == false).ToList();
        }

        callback(new KeyValuePair<File, List<File>>(folder,purgedList));
    }
    
    public static void UpdateFileContents(Action onDownloadFinished, List<File> markedForUploadList = null, File folder = null, List<IEnumerator<float>> coroutineList = null)
    {
        GoogleDriveConfig googleDriveConfig = ServiceHelper.GetConfig().GoogleDriveConfig;

        if (coroutineList == null)
        {
            coroutineList = new List<IEnumerator<float>>();
        }
        
        if(folder == null)
        {
            folder = googleDriveConfig.Folder;
        }
        
        var resources = googleDriveConfig.FileList.Where(f => f.Parents != null && f.Parents.Contains(folder.Id));

        foreach (File resource in resources)
        {
            if (resource.MimeType == Helpers.FolderMimeType)
            {
                UpdateFileContents(null, markedForUploadList, resource, coroutineList);
            } else
            {
                if (resource.Content != null)
                {
                    Debug.Log("Resource content is already up to date");   
                    continue;
                }

                if (markedForUploadList != null && !markedForUploadList.Contains(resource))
                {
                    continue;
                }

                if (resource.MimeType == Helpers.SheetMimeType || resource.MimeType == Helpers.DocumentMimeType || resource.MimeType == Helpers.SlidesMimeType)
                {
                    FileProcessingConfig c = ServiceHelper.GetConfig().FileProcessingConfigs.FirstOrDefault(pfc => pfc.InputMimeType == resource.MimeType);
                    if(c != null)
                    {
                        coroutineList.Add(_ExportRoutine(resource, c.OutputMimeType));
                    } else
                    {
                        ServiceHelper.ShowWarningNotification($"Did not found output MimeType for [{resource.MimeType}]");
                    }
                } else
                {
                    coroutineList.Add(_DownloadRoutine(resource));
                }
            }
        }

        if (onDownloadFinished != null)
        {
            ServiceHelper.RunCoroutine(_RunDownloadCoroutines(coroutineList, onDownloadFinished));
        }
    }

    private static IEnumerator<float> _RunDownloadCoroutines(List<IEnumerator<float>> coroutineList, Action onDownloadFinished)
    {
        int initialCoroutines = coroutineList.Count;
        int downloadedFiles = 0;
        
        ServiceHelper.ShowMessage("Downloading File Contents from Google Drive", true);
        
        while (coroutineList.Count > 0)
        {
            List<IEnumerator<float>> runningCoroutines = coroutineList.GetRange(0, Math.Min(coroutineList.Count, DownloadBatchSize)).ToList();
            
            ServiceHelper.ShowProgressBar($"Downloading files {downloadedFiles}-{downloadedFiles + runningCoroutines.Count} from {initialCoroutines}", downloadedFiles / (float) initialCoroutines);

            //Run Coroutines
            List<CoroutineHandle> runningHandles = new List<CoroutineHandle>();
            foreach (IEnumerator<float> coroutine in runningCoroutines)
            {
                CoroutineHandle handle = ServiceHelper.RunCoroutine(coroutine);
                runningHandles.Add(handle);
            }

            yield return Timing.WaitForSeconds(1.5f);
            
            //Wait for them to finish
            while (runningHandles.Exists(ch => ch.IsRunning))
            {
                yield return 0;
            }

            downloadedFiles += runningCoroutines.Count;
            
            //Remove them from the main list
            foreach (IEnumerator<float> coroutine in runningCoroutines)
            {
                coroutineList.Remove(coroutine);
            }
        }
        
        ServiceHelper.HideMessage();
        onDownloadFinished?.Invoke();
    }

    private static IEnumerator<float> _ExportRoutine(File file, string mimeType)
    {
        bool success = false;
        do
        {
            var exportRequest = GoogleDriveFiles.Export(file.Id, mimeType);
            exportRequest.Send();
            
            while (exportRequest.IsRunning)
            {
                yield return 0;
            }

            if (exportRequest.IsDone)
            {
                file.Content = exportRequest.ResponseData.Content;
                success = true;
            } else
            {
                //Delay before retry to avoid hitting quotas
                yield return Timing.WaitForSeconds(1);
            }
        } while (!success);
    }
    
    private static IEnumerator<float> _DownloadRoutine(File file)
    {
        bool success = false;
        do
        {
            var exportRequest = GoogleDriveFiles.Download(file.Id);
            exportRequest.Send();

            while (exportRequest.IsRunning)
            {
                yield return 0;
            }

            if (exportRequest.IsDone)
            {
                file.Content = exportRequest.ResponseData.Content;
                success = true;
            } else
            {
                //Delay before retry to avoid hitting quotas
                yield return Timing.WaitForSeconds(1);
            }
        } while (!success);
    }
}
