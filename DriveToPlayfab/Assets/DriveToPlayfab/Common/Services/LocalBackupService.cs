﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using _Scripts.Model;
using UnityGoogleDrive;
using File = UnityGoogleDrive.Data.File;

namespace _Scripts.Services {
    public static class LocalBackupService
    {
        public static void SaveLocalBackupFiles(List<File> files, File folder, Action OnFinish = null)
        {
            SaveLocalBackupFiles(files, folder, ServiceHelper.GetConfig().LocalBackupConfig.BackupPath, OnFinish);
        }
    
        private static void SaveLocalBackupFiles(List<File> files, File folder, string path, Action OnFinish = null)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        
            var resources = files.Where(f => f.Parents != null && f.Parents.Contains(folder.Id));

            foreach (File resource in resources)
            {
                if (resource.MimeType == Helpers.FolderMimeType)
                {
                    SaveLocalBackupFiles(files, resource, $"{path}/{resource.Name}");
                } else if(resource.Content != null && resource.Content.Length > 0)
                {
                    FileProcessingConfig c = ServiceHelper.GetConfig().FileProcessingConfigs.FirstOrDefault(fpc => fpc.InputMimeType == resource.MimeType);
                    if (c != null)
                    {
                        SaveFile(resource, c, path);
                    }
                }
            }

            OnFinish?.Invoke();
        }

        private static void SaveFile(File resource, FileProcessingConfig config, string path)
        {
            string newName = resource.Name;
        
            if (ServiceHelper.GetConfig().ConvertFileNamesToLower)
            {
                newName = newName.ToLower();
            }
        
            if (ServiceHelper.GetConfig().ReplaceEmptySpacesForUnderscores)
            {
                newName = newName.Replace(" ", "_");
            }
            
            newName = CleanPathComponent(newName);
            
            string newPath = $"{path}/{newName}.{config.Extension}";
            System.IO.File.WriteAllText(newPath, Encoding.UTF8.GetString(resource.Content));
        }
    
        private static string CleanPathComponent(string pathComponent)
        {
            return string.Join(string.Empty, pathComponent.Split(Path.GetInvalidFileNameChars()));
        }
    }
}
